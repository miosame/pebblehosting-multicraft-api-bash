# Pebblehost Multicraft API Bash

Pebblehost uses the Multicraft API for all their dashboards (incl. discord bot hosting), [their docs](https://help.pebblehost.com/en/article/using-the-pebblehost-game-panel-api-mv0hk4/) are quite short in that regard.

But they refer to the official Multicraft API docs here: https://www.multicraft.org/site/docs/api

These mention the `MulticraftAPI.php` which I've adjusted to work in PHP8 and is added to this repo too - if you want to use it.

However I wanted to use it without php, python, .. so I made a simple bash wrapper for the methods based on what `MulticraftAPI.php` does.

You can adjust it to take in the server-ID and other stuff as arguments/parameters, mine only needs the method, since I don't use any of the others dynamically and therefore they are hardcoded.

```bash
# usage: pebbleAPI <method>

pebbleAPI() {
    SERVER="CHANGEME1"
    API="https://panel.pebblehost.com/api.php"
    USER="CHANGEME2"
    KEY="CHANGEME3"
    METHOD="$1"
    PARAMS="?id=${SERVER}&_MulticraftAPIMethod=${METHOD}&_MulticraftAPIUser=${USER}"
    APIKEY=$(echo -n "${PARAMS}" | sed -e "s/[?&=]//g" | sed -e 's/%40/@/g' | openssl sha256 -hmac "${KEY}" | sed 's/^.* //')

    curl --request GET \
    --url "https://panel.pebblehost.com/api.php${PARAMS}&_MulticraftAPIKey=${APIKEY}" \
    --header 'Referer: https://panel.pebblehost.com' \
    --header 'User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0'
}
```

In order of changes to be done:

- `CHANGEME1` - the server ID can be obtained by going to your pebble game dashboard (incl. discord bot hostings) and the URL will look something like this: `https://panel.pebblehost.com/server/NUMBERS` where it says `NUMBERS` that's what you should change `CHANGEME1` to.

- `CHANGEME2` - in pebble that's your e-mail you login with.

- `CHANGEME3` - that's the API-key you need to generate by going to your game dashboard, clicking on your avatar top right and on the left clicking `Add API key`, it'll generate the key you should replace `CHANGEME3` with.

Don't touch the rest.

That function can be used via bashrc alias too:

```bash
alias reloadserver="pebbleAPI killServer && sleep 5 && pebbleAPI startServer"
```

You may want to use `stopServer` instead of `killServer` for your use-case, but again: I don't need to wait for it to stop myself.
